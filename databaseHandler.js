let Player = require('./model/player.js');
let getPlayerInformation = require('./GetPlayerInformation.js');


async function updateDB(entries) {
    console.log('[STATUS] Updating database...');

    for (player of entries) {
      console.log('[INFO] updating ' + player.uid + "...")
      // let info = {};
      let info = await getPlayerInformation(player.uid);
      let lastSeen = "Hidden in friends list..";
      let online = false;
      let status = "Status not set...";

      if (typeof info.ps === 'undefined') {
        console.log('[INFO] Error! Skipping entry!');
        continue;
      }

      if (typeof info.ps.lastSeen !== 'undefined') {
        let lastSeenDate = info.ps.lastSeen.split("T")[0].split("-");
        let lastSeenTime = info.ps.lastSeen.split("T")[1].split(".")[0];
        let checkMonth = lastSeenDate[1].charAt(0) === '0' ? lastSeenDate[1].split("0")[1] : lastSeenDate[1];
        lastSeen = `${checkMonth}/${lastSeenDate[2]}/${lastSeenDate[0]} | ${lastSeenTime}`;
        online = info.ps.online;
        if(typeof info.ps.status != 'undefined') {
          status = info.ps.status;
        } 
        
      } else {
        status = "Hidden in friends list..";
      }


      let loc = typeof info.ps.location == 'undefined' ? "Player is unfindable or offline" : info.ps.location;

      // YUCK, but these stupid ifs must be done :(
      if (player.name !== info.pi.screenName) await Player.updateOne({ uid: player.uid }, { name: info.pi.screenName });
      if (player.rank !== info.pi.rank) await Player.updateOne({ uid: player.uid }, { rank: info.pi.rank });
      if (player.online !== online) await Player.updateOne({ uid: player.uid }, { online: online });
      if (player.lastSeen !== lastSeen) await Player.updateOne({ uid: player.uid }, { lastSeen: lastSeen });
      if (player.age !== info.pi.ageDays) await Player.updateOne({ uid: player.uid }, { age: info.pi.ageDays });
      if (player.placed !== info.pi.stat_ItemsPlaced) await Player.updateOne({ uid: player.uid }, { placed: info.pi.stat_ItemsPlaced });
      if (player.location !== loc) await Player.updateOne({ uid: player.uid }, { location: loc });
      if (player.minfinity == info.pi.hasMinfinity) await Player.updateOne({ uid: player.uid }, { minfinity: info.pi.hasMinfinity });
      if (player.unfindable !== info.ps.unfindable) await Player.updateOne({ uid: player.uid }, { unfindable: info.ps.unfindable });
      if (player.status !== status) await Player.updateOne({ uid: player.uid }, { status: status })
    }

    console.log('[SUCCESS] Database has been updated!')

    setTimeout(() => updateDB(), 7200000)
  }



  async function applySchema(entries) {
    console.log('[STATUS] Applying schema...');

    for (player of entries) {
      console.log(player)
      console.log('[INFO] updating ' + player.uid + "...")
      // let info = {};
      let info = await getPlayerInformation(player.uid);
      let lastSeen = "Hidden in friends list..";
      let online = false;
      let status = "Status not set...";

      

      if (typeof info.ps === 'undefined') {
        console.log('[INFO] Error! Skipping entry!');
        continue;
      }

      if (typeof info.ps.lastSeen !== 'undefined') {
        let lastSeenDate = info.ps.lastSeen.split("T")[0].split("-");
        let lastSeenTime = info.ps.lastSeen.split("T")[1].split(".")[0];
        let checkMonth = lastSeenDate[1].charAt(0) === '0' ? lastSeenDate[1].split("0")[1] : lastSeenDate[1];
        lastSeen = `${checkMonth}/${lastSeenDate[2]}/${lastSeenDate[0]} | ${lastSeenTime}`;
        online = info.ps.online;
        if(typeof info.ps.status != 'undefined') {
          status = info.ps.status;
        } 
      } else {
        status = "Hidden in friends list..";
      }
      
      let loc = typeof info.ps.location == 'undefined' ? "Player is unfindable or offline" : info.ps.location;

      await Player.deleteMany({uid: player.uid});

      await new Player({
        uid: player.uid,
        messages: player.messages,
        body: player.body,
        name: player.name,
        rank: player.rank,
        online: online,
        lastSeen: lastSeen,
        age: info.pi.ageDays,
        placed: info.pi.stat_ItemsPlaced,
        location: loc,
        minfinity: info.pi.hasMinfinity,
        unfindable: info.ps.unfindable,
        status: status + "",
      }).save();

    }
    console.log('[SUCCESS] Database has been updated!')
  }



async function updateEntry(player) {


    console.log('[INFO] updating ' + player.uid + "...")
    // let info = {};
    let info = await getPlayerInformation(player.uid);
    let lastSeen = "Hidden in friends list..";
    let online = false;
    let status = "Status not set...";
  
    if (typeof info.ps === 'undefined') {
      console.log('[INFO] Error!');
      return;
    }
  
    if (typeof info.ps.lastSeen !== 'undefined') {
      let lastSeenDate = info.ps.lastSeen.split("T")[0].split("-");
      let lastSeenTime = info.ps.lastSeen.split("T")[1].split(".")[0];
      let checkMonth = lastSeenDate[1].charAt(0) === '0' ? lastSeenDate[1].split("0")[1] : lastSeenDate[1];
      lastSeen = `${checkMonth}/${lastSeenDate[2]}/${lastSeenDate[0]} | ${lastSeenTime}`;
      online = info.ps.online;
      if(typeof info.ps.status != 'undefined') {
        status = info.ps.status;
      } 
    } else {
      status = "Hidden in friends list..";
    }
  
  
    let loc = typeof info.ps.location == 'undefined' ? "Player is unfindable or offline" : info.ps.location;
  
    // YUCK, but these stupid ifs must be done :(
    if (player.name !== info.pi.screenName) await Player.updateOne({ uid: player.uid }, { name: info.pi.screenName });
    if (player.rank !== info.pi.rank) await Player.updateOne({ uid: player.uid }, { rank: info.pi.rank });
    if (player.online !== info.ps.online) await Player.updateOne({ uid: player.uid }, { online: online });
    if (player.lastSeen !== info.ps.lastSeen) await Player.updateOne({ uid: player.uid }, { lastSeen: lastSeen });
    if (player.age !== info.pi.ageDays) await Player.updateOne({ uid: player.uid }, { age: info.pi.ageDays });
    if (player.placed !== info.pi.stat_ItemsPlaced) await Player.updateOne({ uid: player.uid }, { placed: info.pi.stat_ItemsPlaced });
    if (player.location !== loc) await Player.updateOne({ uid: player.uid }, { location: loc });
    if (player.minfinity == info.pi.hasMinfinity) await Player.updateOne({ uid: player.uid }, { minfinity: info.pi.hasMinfinity });
    if (player.unfindable !== info.ps.unfindable) await Player.updateOne({ uid: player.uid }, { unfindable: info.ps.unfindable });
    if (player.status !== status) await Player.updateOne({ uid: player.uid }, { status: status })
  }


  module.exports = {
      updateDB: updateDB,
      updateEntry: updateEntry,
      applySchema: applySchema
  }
