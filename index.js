var express = require("express");
let cors = require('cors');
const fs = require("fs").promises;
let mongoose = require("mongoose");
let fetch = require("node-fetch");



const app = express();
app.use(cors());

// this one is secret ;)
let scrape = require("./cookieScraper");
let startDiscordBot = require("./bot.js");

  
async function init() {

    await mongoose.connect('mongodb+srv://pbox:nv2qN5QaPIUoDnqs@cluster0.li2hf.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  console.log("[STATUS] Connected to database");

    console.log("[#] Updating cookies...");
    console.log("---------------------------------")
    await scrape();
    console.log("---------------------------------")
    console.log("[+] Done!");
      

    app.get("/cookies", async function (req, res, next) {
        let rawdata = await fs.readFile("./cookies.json");
        console.log('req')
        res.send(JSON.parse(rawdata));
    });

    setInterval(async () => {
      await fetch("https://pbot-12t6.onrender.com");

      console.log("[INFO] Server pinged!");
    }, 780000);

    console.log("[+] Cookies available!");
      
    app.listen(process.env.PORT || 3126);

    startDiscordBot();
    console.log("[+] Bot online!");
}

init();
