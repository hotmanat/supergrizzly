const { Client, MessageEmbed } = require("discord.js");
const client = new Client();
let Player = require("./model/player.js");
let { updateEntry } = require("./databaseHandler.js");

function createDataEmbed(user, description) {
  let forwardStatus = user.status;
  if (user.status === "") {
    forwardStatus = "Status is empty...";
  }
  let newEmbed = new MessageEmbed()
    .setColor("#08cc94")
    .setTitle(user.name)
    .setDescription(description)
    .addFields(
      { name: "ID", value: user.uid },
      { name: "Name", value: user.name },
      { name: "Rank", value: user.rank },
      { name: "Location", value: user.location },
      { name: "Is Online", value: user.online },
      {
        name: "Last Seen",
        value: `Date: ${user.lastSeen.split(" | ")[0]}\nTime: ${
          user.lastSeen.split(" | ")[1]
        }`,
      },
      { name: "Days Played", value: user.age },
      { name: "Placed Blocks", value: user.placed },
      { name: "Has Minfinity", value: user.minfinity },
      { name: "Is Unfindable", value: user.unfindable },
      { name: "Status", value: forwardStatus }
    )
    .setImage("https://djaii3xne87ak.cloudfront.net/" + user.body);

  return newEmbed;
}

async function responseBook(response, message, page, additionalInfo) {
  let pageArray = [];
  let currentPage = page;
  let currentIndex = -1;
  let previousBreak = 0;

  for (let i = 0; i < response.length; i++) {
    let player = response[i];
    let playerString = additionalInfo
      ? `[${i + 1}] ` +
        player.name +
        ` #Age: ${player.age}#` +
        "\n" +
        "|" +
        player.uid
      : `[${i + 1}] ` + player.name + "\n" + "|" + player.uid;
    if (i == 0 || i == previousBreak + 10) {
      pageArray.push([playerString]);
      previousBreak = i;
      currentIndex++;
    } else {
      pageArray[currentIndex].push(playerString);
    }
  }
  let pages = pageArray.length;
  let players = "";

  for (let player of pageArray[currentPage - 1]) {
    players += player.split("|")[0];
  }
  let newEmbed = new MessageEmbed()
    .setColor("#08cc94")
    .setTitle(`Page ${currentPage} of ` + pages)
    .setDescription(`\`\`\`${players}\`\`\``)
    .setFooter(
      `Use command "~page <pageNumber>" to change which page you're on!\nUse command "~select <entryNumber>" to get players information!`
    );

  message.channel.send(newEmbed);

  let filter = (m) => m.author.id === message.author.id;
  message.channel
    .awaitMessages(filter, {
      max: 1,
      time: 30000,
      errors: ["time"],
    })
    .then(async (message) => {
      message = message.first();

      if (message.content.startsWith("~")) {
        if (message.content.includes("page")) {
          let number = JSON.parse(message.content.split("page")[1]);
          number = Math.round(number);

          if (number >= 1 && number <= pages) {
            currentPage = number;

            await responseBook(response, message, currentPage, additionalInfo);
          } else {
            message.channel.send("Invalid page number!");
          }
        } else if (message.content.includes("select")) {
          let number = JSON.parse(message.content.split("select")[1]);

          let breakFlag = false;

          for (let player of pageArray[currentPage - 1]) {
            let entryNumber = JSON.parse(player.split("[")[1].split("]")[0]);

            if (entryNumber === number) {
              let arg = player.split("|")[1];

              let response = await Player.find({ uid: arg });

              await updateEntry(response[0]);
              response = await Player.find({ uid: arg });

              let user = response[0];
              let compiledData = createDataEmbed(user, "");

              message.channel.send(compiledData);
              breakFlag = true;
              break;
            }
          }

          if (!breakFlag) message.channel.send("Invalid entry number!");
        }
      }
    })
    .catch((collected) => {
      // times up
    });
}

async function updateStatus() {
  let res = await Player.find({});
  client.user.setActivity(`with ${res.length} Manyzen`);

  setTimeout(async () => await updateStatus(), 7200000);
}

function startDiscordBot() {
  client.destroy();
  client.login(process.env.BOT_TOKEN);

  client.on("ready", async () => {
    await updateStatus();
  });

  client.on("guildCreate", (guild) => {
    const channel = guild.channels.cache.find(
      (channel) =>
        channel.type === "text" &&
        channel.permissionsFor(guild.me).has("SEND_MESSAGES")
    );

    let newEmbed = new MessageEmbed()
      .setColor("#08cc94")
      .setTitle("Pandora's Box")
      .setDescription(
        "Pandora's Box allows users to search for nearly every player and receive access to neatly compiled publicly available game information in regards to any given player. If you have any questions or concerns, please contact Zoltar#9917."
      )
      .addFields(
        {
          name: "~name <playerName>",
          value:
            "Searches database by player username. The search is CASE SENSITIVE(note, all names are registered as lowercase) and must include SPACES within the name. When searching by name it is possible more than one value will be returned.",
        },
        {
          name: "~id <playerId>",
          value: "Searches database by player id. Recommended search method.",
        },
        {
          name: "~rank <rank>",
          value:
            "Searches database by player rank. Will return more than 1 entry, unless searching by rank 10.",
        },
        {
          name: "~page <pageNumber>",
          value:
            "Changes current page. Can only be used when page menu is open.",
        },
        {
          name: "~select <entryNumber>",
          value:
            "Gets entries  information. Can only be used when page menu is open.",
        },
        { name: "~help", value: "Opens help menu." }
      )
      .setThumbnail(
        "https://cdn.discordapp.com/attachments/614637022614782000/1080360951397761105/thing.png"
      );

    channel.send(newEmbed);
  });

  client.on('error', err => {
    console.error(err);
    process.exit(1);
  });

  client.on("message", async (message) => {
    if (message.content.startsWith("~")) {
      const type = message.content.substring(1);

      if (type != "") {
        let argument = type.substring(type.indexOf(" ") + 1, type.length);
        let switchCase;
        let flag = false;
        if (type.includes(" ")) {
          switchCase = type.split(" ")[0];
        } else {
          switchCase = type;
          flag = true;
        }
        if (argument != type || flag) {
          try {
            switch (switchCase) {
              case "id": {
                let response = await Player.find({ uid: argument });
                if (response.length === 0) {
                  message.channel.send(
                    "Unable to find entered matching player. The player you are looking for might not be listed in the database, or the id is invalid."
                  );
                  return;
                }
                await updateEntry(response[0]);
                response = await Player.find({ uid: argument });

                let user = response[0];
                let compiledData = createDataEmbed(user, "");

                message.channel.send(compiledData);
                break;
              }

              case "name": {
                let response = await Player.find({ name: argument });
                if (response.length === 0) {
                  message.channel.send(
                    'Unable to find player matching query. The player you are looking for might not be listed in the database, or the name was entered incorrectly. Please review "~help" for proper name syntax.'
                  );
                  return;
                }
                if (response.length > 1) {
                  responseBook(response, message, 1, true);
                } else {
                  await updateEntry(response[0]);
                  response = await Player.find({ uid: response[0].uid });

                  let user = response[0];
                  let compiledData = createDataEmbed(user, "");

                  message.channel.send(compiledData);
                }
                break;
              }

              case "rank": {
                let potential = ["1", "2", "3", "4", "5", "10"];
                if (potential.includes(argument)) {
                  let response = await Player.find({ rank: argument });

                  responseBook(response, message, 1, false);
                } else {
                  message.channel.send("Invalid rank number!");
                }

                break;
              }

              case "body": {
                let response = await Player.find({ body: argument });
                if (response.length === 0) {
                  message.channel.send(
                    "Unable to find player matching query. The player you are looking for might not be listed in the database, or the body id is invalid."
                  );
                  return;
                }
                if (response.length > 1) {
                  responseBook(response, message, 1, true);
                } else {
                  await updateEntry(response[0]);
                  response = await Player.find({ uid: response[0].uid });

                  let user = response[0];
                  let compiledData = createDataEmbed(user, "");

                  message.channel.send(compiledData);
                }
                break;
              }

              case "help": {
                let newEmbed = new MessageEmbed()
                  .setColor("#08cc94")
                  .setTitle("Pandora's Box")
                  .setDescription(
                    "Pandora's Box allows users to search for nearly every player and receive access to neatly compiled publicly available game information in regards to any given player. If you have any questions or concerns, please contact Zoltar#9917."
                  )
                  .addFields(
                    {
                      name: "~name <playerName>",
                      value:
                        "Searches database by player username. The search is CASE SENSITIVE and must include SPACES within the name. When searching by name it is possible more than one value will be returned.",
                    },
                    {
                      name: "~id <playerId>",
                      value:
                        "Searches database by player id. Recommended search method.",
                    },
                    {
                      name: "~rank <rank>",
                      value:
                        "Searches database by player rank. Will return more than 1 entry, unless searching by rank 10.",
                    },
                    {
                      name: "~page <pageNumber>",
                      value:
                        "Changes current page. Can only be used when page menu is open.",
                    },
                    {
                      name: "~select <entryNumber>",
                      value:
                        "Gets entries  information. Can only be used when page menu is open.",
                    },
                    { name: "~help", value: "Opens help menu." }
                  )
                  .setThumbnail(
                    "https://cdn.discordapp.com/attachments/614637022614782000/1080360951397761105/thing.png"
                  );

                message.channel.send(newEmbed);
              }

              case "page":
                break;

              case "select":
                break;

              default:
                message.channel.send(
                  'Invalid syntax, please review proper usage by using the "~help" command...'
                );
                break;
            }
          } catch (err) {
            console.log(err);
            message.channel.send(
              'Something went wrong! This could be a server error or improper usage. Please review syntax with "~help"'
            );
          }
        } else {
          message.channel.send(
            'Invalid syntax, please review proper usage by using the "~help" command...'
          );
          return;
        }
      }
    }
  });
}

module.exports = startDiscordBot;
