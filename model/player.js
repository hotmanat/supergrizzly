let mongoose = require('mongoose');


const PlayerS = new mongoose.Schema({
    uid: String,
    messages: Array,
    body: String,
    rank: Number,
    name: String,
    online: Boolean,
    lastSeen: String,
    age: Number,
    placed: Number,
    location: String,
    minfinity: Boolean,
    unfindable:  Boolean,
    status: String


})
let Player = mongoose.model('player', PlayerS);

module.exports = Player;
