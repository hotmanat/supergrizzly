const puppeteer = require("puppeteer-extra");
const StealthPlugin = require("puppeteer-extra-plugin-stealth");
const fs = require("fs").promises;
puppeteer.use(StealthPlugin());

async function scrape() {
  let finalCookies = {
    cookie1: {
      s: "",
      xcsrf: "",
    },
    cookie2: {
      s: "",
      xcsrf: "",
    },
  };
  // :9

  const args = [
    "--no-sandbox",
    "--disable-setuid-sandbox",
    "--disable-infobars",
    "--window-position=0,0",
    "--ignore-certifcate-errors",
    "--ignore-certifcate-errors-spki-list",
    '--user-agent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3312.0 Safari/537.36"',
    "--enable-features=NetworkService",
  ];

  const options = {
    args,
    headless: true,
  };

  let browser = await puppeteer.launch(options);

  for (let i = 0; i < 2; i++) {
    let pass = i == 0 ? process.env.PASSWORD_ONE : process.env.PASSWORD_TWO;
    let email = i == 0 ? process.env.USERNAME_ONE : process.env.USERNAME_TWO;
    i == 0
      ? console.log("Getting 1st cookie")
      : console.log("Getting 2nd cookie");
    try {
      

      
      let page = await browser.newPage();
      await page.goto("https://manyland.com/login");
      await page.click('a[href="/auth/google"]', { delay: 30 });

      await page.waitForSelector('input[id="identifierId"]');
      await page.type('input[id="identifierId"]', email, {
        delay: 30,
      });
      await page.click("#identifierNext");

      await page.waitForSelector('input[type="password"]', { visible: true });
      await page.type('input[type="password"]', pass, {
        delay: 30,
      });

      await page.waitForSelector("#passwordNext", { visible: true });
      await page.click("#passwordNext", {
        delay: 30,
      });
      console.log('[+] Logged in!')

      await page.waitForTimeout(4000);

      const client = await page.target().createCDPSession();
      // const cookies = (await client.send("Network.getAllCookies")).cookies;
      const cookies = await page.cookies();
      page.on("request", async (response) => {

        const test = await response.headers();
        if (typeof test["x-csrf"] !== "undefined") {
             

          finalCookies[`cookie${i + 1}`].s = cookies.filter(
            (cookie) => cookie.name === "s"
          )[0].value;
          finalCookies[`cookie${i + 1}`].xcsrf = test["x-csrf"];

          if (i == 1) {
            await fs.writeFile(
              __dirname + "/cookies.json",
              JSON.stringify(finalCookies)
            );
            await browser.close();
          } else {
            await client.send('Network.clearBrowserCookies')
            await page.close();
          }
            
        } else {
          // browser.close();
          console.log('[!] Waiting for X-CSRF...')
        }
      });
      await page.waitForTimeout(10000);
    } catch (e) {
      console.log(e);
    }
  }
}

module.exports = scrape;
