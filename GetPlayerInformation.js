let request = require("request-promise-native");
const fs = require("fs").promises;
let cookieJar = request.jar();
let sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

request = request.defaults({ jar: cookieJar });

let ringArray = [
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "10",
  "11",
  "12",
  "13",
  "14",
  "15",
  "16",
  "17",
  "18",
  "19",
  "20",
  "21",
  "22",
  "23",
  "24",
  "25",
  "26",
  "27",
  "28",
  "29",
  "30",
];

async function getPlayerInformation(id) {
  let rawdata = await fs.readFile("./cookies.json");
  let storedCookies = JSON.parse(rawdata);

  let sessionArray = [
    {
      cookie: storedCookies.cookie1.s,
      xcsrf: storedCookies.cookie1.xcsrf,
    },
    {
      cookie: storedCookies.cookie2.s,
      xcsrf: storedCookies.cookie2.xcsrf,
    },
  ];

  let userDataPi;
  let userDataPs;
  // Populate cookie jar
  let session = sessionArray[Math.floor(Math.random() * sessionArray.length)];

  request = request.defaults({
    headers: {
      Cookie: `s=${session.cookie}; Domain=.manyland.com; Path=/; Expires=Sat, 20 June 2050 13:00:01 GMT; HttpOnly`,
      "X-CSRF": session.xcsrf,
    },
  });

  try {
    userDataPi = await request({
      method: "POST",
      url: "http://manyland.com/j/u/pi/",
      form: {
        id: id,
        planeId: 0,
        areaID: "5f163f57be72d462f412bf5d",
      },
      json: true,
    });

    userDataPs = await request({
      method: "GET",
      url: `http://manyland.com/j/u/ps/${id}`,
      json: true,
    });

    if (typeof userDataPs.location != "undefined") {
      if (
        typeof userDataPs.location.a === "number" ||
        ringArray.includes(userDataPs.location.a)
      ) {
        userDataPs.location = userDataPs.location.a.toString();
      } else {
        await sleep(1000);

        let area = await request({
          method: "GET",
          url: `http://manyland.com/j/a/nad/${userDataPs.location.a}`,
          json: true,
        });

        userDataPs.location = area.name;
      }
    }
  } catch (e) {
    console.log("failed: scraping new cookies");
    await getPlayerInformation(id);
  }

  let data = {
    pi: userDataPi,
    ps: userDataPs,
  };

  return data;
}

module.exports = getPlayerInformation;
